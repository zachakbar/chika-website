(function($) {
	'use strict';

	const $body = $('body'),
				$header = $('header[role="banner"]'),
				$main = $('section[role="main"]');

	$(document).ready(function() {
		AOS.init({
			once: true,
			duration: 700
		});

		var s = skrollr.init({
			mobileCheck: function() {
				//hack - forces mobile version to be off
				return false;
			}
		});

		init.ready();

		if ( $(window).width() < 768 ) {
			$('.btn-3d').addClass('is-active');
		}
		if ( $(window).width() < 900 ) {
			$('ul.main-menu li.menu-item:nth-child(4)').addClass('has-large-svg');
			$('ul.main-menu li.menu-item:last-of-type').addClass('has-large-svg');
		}
		if ( $(window).height() < 600 ) {
			$('section.hero').addClass('low-profile');
		}

		$(window).resize(function(){
			if ( $(window).width() >= 768 ) {
				$('.btn-3d').removeClass('is-active');
			}else{
				$('.btn-3d').addClass('is-active');
			}
			if ( $(window).width() >= 900 ) {
				$('ul.main-menu li.menu-item:nth-child(4)').removeClass('has-large-svg');
				$('ul.main-menu li.menu-item:last-of-type').removeClass('has-large-svg');
			} else {
				$('ul.main-menu li.menu-item:nth-child(4)').addClass('has-large-svg');
				$('ul.main-menu li.menu-item:last-of-type').addClass('has-large-svg');
			}
			if ( $(window).height() < 600 ) {
				$('section.hero').addClass('low-profile');
			}else{
				$('section.hero').removeClass('low-profile');
			}
		});

	});

	$(window).on('load',function() {
		//init.load();
	});

	var init = {

		/**
		* Call functions when document ready
		*/
		ready: function() {
			this.menuToggle();
			this.activePage();
			this.headerScrollActions();
			this.smoothScroll();
			this.imageSlider();
			this.imageGallery( '.image-gallery-container' );
			this.imageGallery( '.menu-gallery-container-plate', '.swiper-pagination-plate' );
			this.imageGallery( '.menu-gallery-container-salsas', '.swiper-pagination-salsas' );
			this.contactForm();
		},

		/**
		* Call functions when window load.
		*/
		load: function() {
			//this.FUNCTION_NAME();
		},

		// CUSTOM FUNCTIONS BELOW

		menuToggle: function() {
		  var $toggle = $('.menu-toggle');
		  $toggle.on('click', function() {
		    // Toggle class "is-active"
		    $toggle.toggleClass('is-active');
		    // Do something else, like open/close menu
		    $body.toggleClass('menu-active');
		    $header.toggleClass('is-menu-open');
		  });
		},

		activePage: function() {
			var pageID = '#menu_item_'+$main.data('page');
			$(pageID).addClass('active');
		},

		headerScrollActions: function() {
			$(window).scroll(function(){
				var scroll = $(window).scrollTop();
				if( scroll > 0 ) {
					$header.addClass('is-scrolling');
				}else{
					$header.removeClass('is-scrolling');
				}
				if( scroll > 75 ) {
					$header.addClass('is-shrink');
				}else{
					$header.removeClass('is-shrink');
				}
			});
		},

		smoothScroll: function() {
			var $headHeight = $header.outerHeight();
			$('a[href*="#"]:not([href="#"])').click(function() {
				if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
					var target = $(this.hash);
					target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
					if (target.length) {
						$('html,body').animate({
							scrollTop: target.offset().top - $headHeight
						}, 1000);
						return false;
					}
				}
			});
		},

		imageSlider: function() {
		  var swiper = new Swiper ('.image-slider-container', {
				centeredSlides: true,
				slidesPerView: 'auto',
				pagination: {
					el: '.swiper-pagination-image-slider',
					clickable: true,
				},
		  });
		},

		imageGallery: function( container, pagination_class = '.swiper-pagination' ) {
		  var swiper = new Swiper (container, {
				//centeredSlides: true,
				slidesPerView: 'auto',
				spaceBetween: 0,
				freeMode: true,
				pagination: {
					el: pagination_class,
					clickable: true,
				},
	      navigation: {
	        nextEl: '.swiper-button-next',
	        prevEl: '.swiper-button-prev',
	      },
		  });
		},

		contactForm: function() {
			$('#contact_form').on('submit', function(e){
				//e.preventDefault();
				$.ajax({
					type: 'POST',
					url: 'http://chika.capacitr.com/lib/functions/form-submit.php',
					data: $('form').serialize(),
					dataType: 'html',
					beforeSend: function ( data ) {
						$('#contact_form').hide();
						$('.loading').show();
					},
					success: function () {
						//console.log($(this).serialize());
						$('.loading').hide();
						$('.thank-you').fadeIn('fast');
					},
					error: function(xhr, ajaxOptions, thrownError){
						console.log(xhr.status);
					},
				});
			});
		}

	};

})(jQuery);