<?php
	include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/functions/helpers.php' );
	include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/general/header.php' );
?>

<section role="main" class="page-about" data-page="about">

	<?php

		include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/about/hero.php' );
		include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/general/banner-tiles.php' );
		include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/about/overview.php' );
		include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/about/fullwidth-image.php' );

	?>

</section>

<?php include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/general/footer.php' ); ?>