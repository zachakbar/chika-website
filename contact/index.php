<?php
	include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/functions/helpers.php' );
	include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/general/header.php' );
?>

<section role="main" class="contact" data-page="contact">

	<?php

		include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/contact/hero.php' );
		include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/general/banner-tiles.php' );
		include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/contact/overview.php' );
		include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/contact/form.php' );

	?>

</section>

<?php include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/general/footer.php' ); ?>