<?php
	include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/functions/helpers.php' );
	include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/general/header.php' );
?>

<section role="main" class="home" data-page="home">

	<?php

		include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/home/hero.php' );
		include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/general/banner-tiles.php' );

	?>

	<section role="main">
		<?php
			include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/home/overview.php' );
			include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/home/fullwidth-image.php' );
			include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/home/gallery.php' );
		?>
	</section>

</section>

<?php include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/general/footer.php' ); ?>