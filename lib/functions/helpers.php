<?php

/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/

/* Site Vars/Constants
 *
 */
// set the site url base
$dev = 'http://chika.local';
$staging = 'http://chika.capacitr.com';
$prod = 'https://eatchika.com';
define( 'SITE_URL', $prod );
define( 'IMG_PATH', SITE_URL.'/assets/img/' );

/* simple helper function to return out an svg.
 *
 * @param string $svg: SVG file name
 */
function svg_path( $svg ){
	$svg_path = $_SERVER['DOCUMENT_ROOT'].'/assets/svg/';
	//echo $svg_path.$svg.'.svg';
	return file_get_contents( $svg_path . $svg . '.svg' );
}

function btn_3d( $text, $link, $cls = '' ) {
	$btn = '';
	$btn .= '<div class="btn-3d '.$cls.'">';
		$btn .= '<a href="'.$link.'" class="btn">'.$text.'</a>';
		$btn .= '<div class="left"></div>';
		$btn .= '<div class="left alt"></div>';
		$btn .= '<div class="bottom"></div>';
		$btn .= '<div class="bottom alt"></div>';
	$btn .= '</div>';

	return $btn;
}

function submit_3d( $text, $cls = '', $id = '' ) {
	$btn = '';
	$btn .= '<div class="btn-3d '.$cls.'">';
		$btn .= '<input type="submit" value="'.$text.'" id="'.$id.'" class="btn btn-submit" />';
		$btn .= '<div class="left"></div>';
		$btn .= '<div class="left alt"></div>';
		$btn .= '<div class="bottom"></div>';
		$btn .= '<div class="bottom alt"></div>';
	$btn .= '</div>';

	return $btn;
}

function get_page_name( $sep = '' ) {
	$page = str_replace( '/', '', $_SERVER['REQUEST_URI'] );
	if( $page != '' ):
		return ucfirst($page) . $sep;
	endif;
}

function get_instagram_data() {

	$fields = 'id,username,caption,media_url,permalink';
	$access_token = 'IGQVJYei1mUHpsMm9JTWQ3QzJHSVkta1ZAaMFczSTJ6aHJJN2ttbVJ5aFBZATnNaMXhkcUJGU3E4VDJlbDFlSlhsTHVDbkhqWklGTDhVcEprQk5RcDFIWTBvdTRmbV92azN4aHB4Qm5n';
	$refresh = refresh_access_token( $access_token );
	$access_token = $refresh->access_token;
	$url = 'https://graph.instagram.com/me/media?fields='.$fields.'&access_token='.$access_token;
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json') );
	$data = curl_exec($ch);
	curl_close($ch);

	$json = json_decode($data);

	return $json;
}

function refresh_access_token( $token ) {
	$url = 'https://graph.instagram.com/refresh_access_token?grant_type=ig_refresh_token&access_token='.$token;
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json') );
	$data = curl_exec($ch);
	curl_close($ch);
	$json = json_decode($data);
	return $json;
}

function instagram_filter_by_hashtag() {
	// not sure yet
}