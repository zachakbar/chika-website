<?php include_once($_SERVER['DOCUMENT_ROOT'].'/lib/functions/helpers.php'); ?>
<section class="hero bgcolor aligncenter">
	<div class="wrap">
		<div class="section-content">
			<div class="split-content">
				<div>
					<h1 alt="The Chika Story" data-aos="fade-up"><?php echo svg_path( 'txt-the-chika-story' ); ?></h1>
					<img src="<?php echo IMG_PATH; ?>gfx-hero-about-pollo.png" class="pollo is-hide-sm" />
				</div>
				<div class="section-bg">
					<img src="<?php echo IMG_PATH; ?>gfx-hero-about-granny.jpg" class="granny" />
				</div>
		</div>
	</div>
</section>