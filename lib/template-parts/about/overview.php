<section class="overview page-block">
	<div class="wrap-outer">
		<div class="section-content">
			<div class="split-content">
				<article>
					<img src="<?php echo IMG_PATH; ?>gfx-about-overview-corner.png" class="corner is-hide-sm" />
					<div data-aos="fade-left">
						<h2>Mexico City. 1960<small>s</small></h2>
						<p>You don’t forget someone like Mama Chika.  She had a very specific type of flair. The type that would cook you the greatest meal of your life, drink you under the table while dancing all night, and then take all your money playing cards until the sun came up.</p>
						<p>In a time when recipes weren’t up and down your social media feed, her cooking became the stuff of legend. Most of her signature recipes were written on the back of old casino matchbooks—in immaculate handwriting, of course. Right next to some of her favorite dirty jokes.</p>
						<p>No one was entirely sure how Mama made anything. No one was even sure if she ever did it the same way twice. But they couldn’t get her flavors out of their heads. No one did it like Mama.  She was a flame that burned hot—and she hasn’t lost a step. This Mama still knows how to live, how to cook, and how to bring a little fire to everything she touches.</p>
					</div>
					<div data-aos="fade-up">
						<?php echo btn_3d( 'Try Mama\'s Cooking', '/order/', 'is-large has-accent-orange' ); ?>
					</div>
				</article>
				<div class="image-slider">
					<img src="<?php echo IMG_PATH; ?>Chika-Flyers.png" class="corner is-hide-sm" />
					<!-- Slider main container -->
					<div class="image-slider-container">
					  <!-- Additional required wrapper -->
					  <div class="swiper-wrapper">
					      <!-- Slides -->
					      <div class="swiper-slide"><img src="<?php echo IMG_PATH; ?>placeholder-overview-slider-01.jpg" /></div>
					      <div class="swiper-slide wide"><img src="<?php echo IMG_PATH; ?>placeholder-overview-slider-02.jpg" /></div>
					      <div class="swiper-slide"><img src="<?php echo IMG_PATH; ?>placeholder-overview-slider-03.jpg" /></div>
					  </div>
					</div>
				  <!-- If we need pagination -->
				  <div class="swiper-pagination-image-slider"></div>
				</div>
			</div>
		</div>
	</div>
</section>