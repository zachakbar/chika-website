<section class="form page-block">
	<div class="wrap">
		<div class="section-content">
			<div class="split-content">
				<div class="form-lead-in" data-aos="fade-right">
					<h2>Call your Mama</h2>
					<p>Mama Chika emails now, too. Send feedback, questions, or just say Hi!’ by using the form, or sending an email to <a href="mailto:hi@getchika.com" class="chika-style" target="_blank">hi@getchika.com</a>.</p>
				</div>
				<div data-aos="fade-left" data-aos-delay="200">
					<form action="https://eatchika.us17.list-manage.com/subscribe/post?u=341890e915d213b8cfc17434c&amp;id=f2e32eace1" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
						<p class="mc-field-group">
							<label for="mce-FNAME">First Name </label>
							<input type="text" value="" name="FNAME" class="" id="mce-FNAME">
						</p>
						<p class="mc-field-group">
							<label for="mce-LNAME">Last Name </label>
							<input type="text" value="" name="LNAME" class="" id="mce-LNAME">
						</p>
						<p class="mc-field-group">
							<label for="mce-EMAIL">Email Address <span class="asterisk">*</span></label>
							<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
						</p>
						<!--<p>
							<label>Your Message</label>
							<textarea name="message" id="message" rows="8"></textarea>
						</p>-->

						<p id="mce-responses" class="clear">
							<div class="response" id="mce-error-response" style="display:none"></div>
							<div class="response" id="mce-success-response" style="display:none"></div>
						</p>

						<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
					  <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_341890e915d213b8cfc17434c_f2e32eace1" tabindex="-1" value=""></div>

						<?php echo submit_3d( 'Send Message', 'has-accent-orange is-large', 'mc-embedded-subscribe' ); ?>

					</form>
					<div class="loading">
						<p>Contacting Mama...</p>
					</div>
					<div class="thank-you">
						<h2>Mama is reading<br>your note</h2>
						<p>She prefers handwritten, but she’ll respond soon nonetheless. Thank you for reaching out!</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>