<?php include_once($_SERVER['DOCUMENT_ROOT'].'/lib/functions/helpers.php'); ?>
<section class="hero bgcolor aligncenter">
	<div class="wrap">
		<div class="section-content">
			<h1 alt="El Hermoso Pollo" data-aos="fade-up">
				<span class="is-hide-sm"><?php echo svg_path( 'txt-hasta-la-proxima-vez-lg' ); ?></span>
				<span class="is-hide-lg"><?php echo svg_path( 'txt-hasta-la-proxima-vez-sm' ); ?></span>
			</h1>
		</div>
	</div>
	<div class="section-bg">
		<img src="<?php echo IMG_PATH; ?>gfx-hero-contact-woman-chicken-lighter.jpg" class="woman-chicken-lighter" />
	</div>
</section>