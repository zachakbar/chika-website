<section class="overview page-block">
	<div class="wrap">
		<div class="section-content">
			<div class="split-content">
				<div class="contact-details" data-aos="fade-in">
					<h2>Contact</h2>
					<div class="detail-block">
						<h3>Phone</h3>
						<p><a href="tel:9544148336" class="chika-style" target="_blank">954-414-8336</a></p>
					</div>
					<div class="detail-block">
						<h3>Email</h3>
						<p><a href="mailto:hi@getchika.com" class="chika-style" target="_blank">hi@getchika.com</a></p>
					</div>
					<div class="detail-block">
						<h3>Hours</h3>
						<p>
							Sunday-Thursday 5-10pm<br>
							Friday-Saturday 5-11pm
						</p>
					</div>
				</div>
				<div class="section-graphics">
					<div class="chika-block">
						<img src="<?php echo IMG_PATH; ?>gfx-chat-bubble-muy-delicioso.png" class="chat-bubble" data-aos="zoom-in" data-aos-delay="1000" data-bottom-top="transform: translateY(-15%);" data-top-bottom="transform: translateY(15%);" />
						<img src="<?php echo IMG_PATH; ?>gfx-chika-lady-01.png" class="chika-lady" data-aos="flip-right" data-aos-delay="0" data-bottom-top="transform: translateY(-15%);" data-top-bottom="transform: translateY(15%);" />
					</div>
					<div class="chika-block">
						<img src="<?php echo IMG_PATH; ?>gfx-chat-bubble-gracias.png" class="chat-bubble" data-aos="zoom-in" data-aos-delay="1500" data-bottom-top="transform: translateY(-15%);" data-top-bottom="transform: translateY(15%);" />
						<img src="<?php echo IMG_PATH; ?>gfx-chika-lady-02.png" class="chika-lady" data-aos="flip-right" data-aos-delay="500" data-bottom-top="transform: translateY(-15%);" data-top-bottom="transform: translateY(15%);" />
					</div>
				</div>
			</div>
		</div>
	</div>
</section>