<?php include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/functions/helpers.php' ); ?>

	<?php include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/general/instagram-feed.php' ); ?>

	<footer role="contentinfo">
		<div class="wrap">
			<div class="split-content">
				<div class="hours" data-aos="fade-up">
					<h4>Delivery Hours</h4>
					<p>
						Sunday - Thursday		5:30-10<br>
						Friday - Saturday		5:30-11
					</p>
					<?php echo btn_3d( 'Order Now', '/order/', 'has-accent-white' ); ?>
				</div>
				<div class="logo" data-aos="fade-up">
					<a href="#top"><?php echo svg_path( 'logo-chika-footer' ); ?></a>
				</div>
				<div class="social" data-aos="fade-up">
					<h4>Follow Us On Social</h4>
					<ul>
						<li><a href="https://www.facebook.com/eatchika/" title="Like us on Facebook" target="_blank"><?php echo svg_path('social-facebook'); ?></a></li>
						<li><a href="https://twitter.com/eatchika" title="Follow us on Twitter" target="_blank"><?php echo svg_path('social-twitter'); ?></a></li>
						<li><a href="https://www.instagram.com/eatchika/" title="Follow us on Instagram" target="_blank"><?php echo svg_path('social-instagram'); ?></a></li>
						<li><a href="https://open.spotify.com/user/uvrlpkbprx8pzic3oj5wzr6vs?si=HDTmK7OORiGxbh_A7akXDw" title="Follow us on Spotify" target="_blank"><?php echo svg_path('social-spotify'); ?></a></li>
					</ul>
				</div>
		</div>
	</footer>

</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<script src="/assets/js/scripts.js"></script>

</body>
</html>
