<?php include_once($_SERVER['DOCUMENT_ROOT'].'/lib/functions/helpers.php'); ?>
<!doctype html>
<html lang="en">
<head>

<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="x-ua-compatible" content="ie=edge" />

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/general/meta.php'); ?>

<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:ital,wght@0,300;0,400;0,500;1,400&display=swap" rel="stylesheet">

<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
<link rel="stylesheet" href="/assets/css/styles.css">

<link rel="shortcut icon" href="/assets/img/chika-favicon.png">

</head>

<body>
<a id="top"></a>
<div id="" class="container">

	<header role="banner">
		<div class="wrap">
			<div class="logo-wrap">
				<a class="logo" href="/"><?php echo svg_path( 'logo-chika-header' ); ?></a>
			</div>
			<button class="menu-toggle"><?php echo svg_path( 'menu' ); ?></button>
			<nav role="navigation">
				<ul class="main-menu">
					<li id="menu_item_about" class="menu-item"><a href="/about/">About<?php echo svg_path( 'menu-item-active' ); ?></a></li>
					<li id="menu_item_menu" class="menu-item"><a href="/menu/">Menu<?php echo svg_path( 'menu-item-active' ); ?></a></li>
					<li class="spacer"></li>
					<li id="menu_item_listen" class="menu-item"><a href="/listen/">Listen<?php echo svg_path( 'menu-item-active' ); ?></a></li>
					<li id="menu_item_contact" class="menu-item"><a href="/contact/">Contact<?php echo svg_path( 'menu-item-active' ); ?></a></li>
				</ul>
			</nav>
		</div>
	</header>