<?php include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/functions/helpers.php' ); ?>

<section class="instagram-feed page-block">
	<div class="wrap">
		<div class="section-content">
			<header data-aos="fade-right">
				<?php
					echo svg_path( 'shape-01' );
					echo svg_path( 'txt-instagram' );
					echo svg_path( 'shape-01' );
				?>
			</header>
			<div class="image-blocks">
				<?php
					$data = get_instagram_data();
					$posts = array_slice( $data->data, 0, 4 );
					foreach( $posts as $post => $content ):
						echo '<div data-aos="fade-up">';
							echo '<a href="'.$content->permalink.'" target="_blank">';
								echo '<img src="'.$content->media_url.'" title="'.$content->caption.'" />';
								echo '<div class="overlay"><p>'.$content->caption.'</p></div>';
							echo '</a>';
						echo '</div>';
					endforeach;
				?>
			</div>
		</div>
	</div>
</section>