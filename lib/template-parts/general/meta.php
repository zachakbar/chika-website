<?php include_once($_SERVER['DOCUMENT_ROOT'].'/lib/functions/helpers.php'); ?>
<!-- Primary Meta Tags -->
<?php $title = 'Chika a gourmet Mexican rosticeria.'; ?>
<title><?php echo get_page_name( ' - ' ) . $title; ?></title>
<meta name="title" content="<?php echo get_page_name( ' - ' ) . $title; ?>" />
<meta name="description" content="Delivered with a kick. Chicken flame-roasted to perfection in Mama’s secret herbs. A hit of citrus, and mild peppers give it a kick you’ll come back for." />

<!-- Open Graph / Facebook -->
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://getchika.com/" />
<meta property="og:site_name" content="<?php echo $title; ?>" />
<meta property="og:title" content="<?php echo get_page_name( ' - ' ) . $title; ?>" />
<meta property="og:description" content="Delivered with a kick. Chicken flame-roasted to perfection in Mama’s secret herbs. A hit of citrus, and mild peppers give it a kick you’ll come back for." />
<meta property="og:image" content="/assets/img/chika-social-image.jpg" />
<meta property="og:image:secure_url" content="/assets/img/chika-social-image.jpg" />
<meta property="og:image:width" content="1200" />
<meta property="og:image:height" content="630" />

<!-- Twitter -->
<meta property="twitter:card" content="summary_large_image" />
<meta property="twitter:url" content="https://getchika.com/" />
<meta name="twitter:description" content="Delivered with a kick. Chicken flame-roasted to perfection in Mama’s secret herbs. A hit of citrus, and mild peppers give it a kick you’ll come back for." />
<meta name="twitter:title" content="<?php echo get_page_name( ' - ' ) . $title; ?>" />
<meta name="twitter:site" content="@eatchika" />
<meta name="twitter:image" content="/assets/img/chika-social-image.jpg" />
<meta name="twitter:creator" content="@eatchika" />