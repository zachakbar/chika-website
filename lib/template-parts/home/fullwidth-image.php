<section class="fullwidth-image page-block">
	<div class="wrap-outer">
		<div class="section-content" data-aos="fade-up">
			<?php echo svg_path( 'muy-delicioso' ); ?>
		</div>
		<div class="section-bg left-top">
			<img src="<?php echo IMG_PATH; ?>HP-TrunkChickens.jpg" class="" />
		</div>
	</div>
</section>