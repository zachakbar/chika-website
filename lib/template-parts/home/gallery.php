<section class="gallery page-block">
	<div class="wrap">
		<div class="section-content">
		  <div class="image-gallery-container" data-aos="fade-up">
		    <div class="swiper-wrapper">
		      <div class="swiper-slide"><img src="<?php echo IMG_PATH; ?>placeholder-home-gallery-01.jpg" /></div>
		      <div class="swiper-slide"><img src="<?php echo IMG_PATH; ?>placeholder-home-gallery-02.jpg" /></div>
		    </div>
		    <!-- Add Arrows -->
		    <div class="swiper-button-next is-hide-sm"></div>
		    <div class="swiper-button-prev is-hide-sm"></div>
		  </div>
		  <!-- If we need pagination -->
		  <div class="swiper-pagination is-hide-lg"></div>
		</div>
	</div>
</section>