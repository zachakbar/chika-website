<?php include_once($_SERVER['DOCUMENT_ROOT'].'/lib/functions/helpers.php'); ?>
<section class="hero hero-home bgcolor aligncenter">
	<div class="wrap">
		<div class="section-content">
			<h1 alt="El Hermoso Pollo" data-aos="fade-up"><?php echo svg_path( 'txt-el-hermoso-pollo' ); ?></h1>
			<p data-aos="fade-up" data-aos-delay="100">A gourmet Mexican rosticeria. Delivered with a kick.</p>
		</div>
	</div>
	<div class="section-bg">
		<?php /*
		<img src="<?php echo IMG_PATH; ?>gfx-hero-home-produce-lg.png" class="produce lg is-hide-sm" />
		<img src="<?php echo IMG_PATH; ?>gfx-hero-home-produce-sm.png" class="produce sm is-hide-lg" />
		*/ ?>
		<img src="<?php echo IMG_PATH; ?>gfx-hero-home-pollo-lg.jpg" class="pollo lg is-hide-sm" />
		<img src="<?php echo IMG_PATH; ?>gfx-hero-home-pollo-sm.jpg" class="pollo sm is-hide-lg" />
	</div>
</section>