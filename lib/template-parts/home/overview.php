<section class="overview page-block">
	<div class="wrap-outer">
		<div class="section-content">
			<div class="split-content">
				<div class="image-slider">
					<img src="<?php echo IMG_PATH; ?>Chika-Flyers.png" class="corner is-hide-sm" />
					<!-- Slider main container -->
					<div class="image-slider-container">
					  <!-- Additional required wrapper -->
					  <div class="swiper-wrapper">
					      <!-- Slides -->
					      <div class="swiper-slide"><img src="<?php echo IMG_PATH; ?>placeholder-overview-slider-01.jpg" /></div>
					      <div class="swiper-slide wide"><img src="<?php echo IMG_PATH; ?>placeholder-overview-slider-02.jpg" /></div>
					      <div class="swiper-slide"><img src="<?php echo IMG_PATH; ?>placeholder-overview-slider-03.jpg" /></div>
					  </div>
					</div>
				  <!-- If we need pagination -->
				  <div class="swiper-pagination-image-slider"></div>
				</div>
				<article>
					<img src="<?php echo IMG_PATH; ?>gfx-home-overview-corner.png" class="corner" />
					<h2 data-aos="fade-left">Hola Chika</h2>
					<p data-aos="fade-left">A little bit of nice, with an extra bit of spice. Mama Chika’s flame-roasted birds have a flavor like you’ve never experienced.</p>
					<div data-aos="fade-up">
						<?php echo btn_3d( 'Order Now', '/order/', 'is-large has-accent-orange' ); ?>
					</div>
				</article>
			</div>
		</div>
	</div>
</section>