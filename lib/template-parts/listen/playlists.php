<section class="playlists page-block">
	<div class="wrap">
		<div class="section-content">
			<div class="split-content">
				<div class="section-title">
					<h2>Salsa<br>for you</h2>
					<p>Mama says: Move your<br>feet while you eat</p>
					<div class="chika-lady-group" data-aos="zoom-out" data-bottom-top="transform: translateY(-15%);" data-top-bottom="transform: translateY(15%);">
						<img src="<?php echo IMG_PATH; ?>gfx-chat-bubble-vamonos.png" class="chat-bubble is-hide-sm" />
						<img src="<?php echo IMG_PATH; ?>gfx-listen-playlists-pollo.png" class="chika-lady is-hide-sm" />
					</div>
				</div>
				<div class="playlist-items">
					<!-- Playlist 1 -->
					<div class="playlist-item" data-aos="fade-up" data-aos-delay="0">
						<iframe src="https://open.spotify.com/embed/playlist/2JRKVjsy3nuzmWPzzHWek0" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
						<h3>Playlist Title</h3>
						<p>Playlist music description goes here</p>
					</div>
					<!-- Playlist 2 -->
					<div class="playlist-item" data-aos="fade-up" data-aos-delay="100">
						<iframe src="https://open.spotify.com/embed/playlist/4BNmWb0gVRttWENvhvWzt3" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
						<h3>Playlist Title</h3>
						<p>Playlist music description goes here</p>
					</div>
					<!-- Playlist 3 -->
					<div class="playlist-item" data-aos="fade-up" data-aos-delay="0">
						<iframe src="https://open.spotify.com/embed/playlist/6tQoIqZ6wOzd7DT6eVvv89" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
						<h3>Playlist Title</h3>
						<p>Playlist music description goes here</p>
					</div>
					<!-- Playlist 4 -->
					<div class="playlist-item" data-aos="fade-up" data-aos-delay="100">
						<iframe src="https://open.spotify.com/embed/playlist/5bMHI2jzwpJULeP7RcXt2T" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
						<h3>Playlist Title</h3>
						<p>Playlist music description goes here</p>
					</div>
					<!-- Playlist 5 -->
					<div class="playlist-item" data-aos="fade-up" data-aos-delay="0">
						<iframe src="https://open.spotify.com/embed/playlist/0NAP9tfKj9qUgEpYBtTON8" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
						<h3>Playlist Title</h3>
						<p>Playlist music description goes here</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>