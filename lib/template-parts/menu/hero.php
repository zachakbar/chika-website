<?php include_once($_SERVER['DOCUMENT_ROOT'].'/lib/functions/helpers.php'); ?>
<section class="hero">
	<div class="wrap">
		<div class="section-content">
			<h1 alt="Menu" data-aos="fade-up"><?php echo svg_path( 'txt-menu' ); ?></h1>
		</div>
	</div>
	<div class="section-bg">
		<img src="<?php echo IMG_PATH; ?>bg-hero-menu.jpg"+ />
	</div>
</section>