<section class="menu-row beverages page-block">
	<div class="wrap">
		<div class="section-content">
			<div class="split-content">
				<div class="menu-title">
					<h2 data-aos="fade-right">Beer &amp;<br>Margaritas</h2>
				</div>
				<div class="menu-items">
					<div class="menu-item slim" data-aos="fade-up">
						<h3>
							<span class="name">margarita one</span>
							<span class="price">$16.50</span>
						</h3>
						<p class="ingredients">Salsa Verde, Salsa Roja Asada</p>
					</div>
					<div class="menu-item slim" data-aos="fade-up">
						<h3>
							<span class="name">margarita two</span>
							<span class="price">$16.50</span>
						</h3>
						<p class="ingredients">Salsa Verde, Salsa Roja Asada</p>
					</div>
					<div class="menu-item slim" data-aos="fade-up">
						<h3>
							<span class="name">margarita three</span>
							<span class="price">$16.50</span>
						</h3>
						<p class="ingredients">Salsa Verde, Salsa Roja Asada</p>
					</div>
					<div class="menu-item slim" data-aos="fade-up">
						<h3>
							<span class="name">chika beer</span>
							<span class="price">$16.50</span>
						</h3>
						<p class="ingredients">Salsa Verde, Salsa Roja Asada</p>
					</div>
					<div class="menu-item slim" data-aos="fade-up">
						<h3>
							<span class="name">chika beer</span>
							<span class="price">$16.50</span>
						</h3>
						<p class="ingredients">Salsa Verde, Salsa Roja Asada</p>
					</div>
					<div class="menu-item slim" data-aos="fade-up">
						<h3>
							<span class="name">chika beer</span>
							<span class="price">$16.50</span>
						</h3>
						<p class="ingredients">Salsa Verde, Salsa Roja Asada</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>