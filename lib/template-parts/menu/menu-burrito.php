<section class="menu-row burrito page-block">
	<div class="wrap">
		<div class="section-content">
			<div class="split-content">
				<div class="menu-title">
					<h2 data-aos="fade-right">Burrito</h2>
					<img src="<?php echo IMG_PATH; ?>gfx-menu-chika-woman-02.jpg" id="chika_02" class="chika-woman is-hide-sm" data-aos="zoom-out" data-bottom-top="transform: translateY(-15%);" data-top-bottom="transform: translateY(15%);" />
				</div>
				<div class="menu-items">
					<div class="menu-item" data-aos="fade-up">
						<h3>
							<span class="name">chika burrito</span>
							<span class="price">$16.50</span>
						</h3>
						<p class="description">All Mama’s best, wrapped up just for you.</p>
						<p class="ingredients">Mexican Rice, Black or Pinto Beans, Chika Fire Roasted Chicken, Sauteed Cauliflower, Queso Fresco, Sauteed Cactus, Pico de Gallo, Chipotle Cream, Fried Potato Strings, Chips, Add Guacamole +3</p>
					</div>
					<div class="menu-gallery" data-aos="fade-up">
					  <div class="menu-gallery-container-salsas image-gallery">
					    <div class="swiper-wrapper">
					      <div class="swiper-slide"><img src="<?php echo IMG_PATH; ?>placeholder-home-gallery-01.jpg" /></div>
					      <div class="swiper-slide"><img src="<?php echo IMG_PATH; ?>placeholder-home-gallery-02.jpg" /></div>
					    </div>
					    <!-- Add Arrows -->
					    <div class="swiper-button-next is-hide-sm"></div>
					    <div class="swiper-button-prev is-hide-sm"></div>
					  </div>
					  <!-- If we need pagination -->
					  <div class="swiper-pagination swiper-pagination-salsas is-hide-lg"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>