<section class="menu-row lead-in page-block">
	<div class="wrap">
		<div class="section-content">
			<div class="split-content">
				<div><?php // silence ?></div>
				<div>
					<h3>Chicken flame-roasted to perfection in Mama’s secret herbs. A hit of citrus, and mild peppers give it a kick you’ll come back for.</h3>
				</div>
			</div>
		</div>
	</div>
</section>