<section class="menu-row wings page-block">
	<div class="wrap">
		<div class="section-content">
			<div class="split-content">
				<div class="menu-title">
					<h2 data-aos="fade-right">Misc</h2>
				</div>
				<div class="menu-items">
					<div class="menu-item" data-aos="fade-up">
						<h3>
							<span class="name">chika sandwich</span>
							<span class="price">$16.50</span>
						</h3>
						<p class="description">Made with a Mama’s love.</p>
						<p class="ingredients">Chika flame-roasted chicken, salsa macha mayonnaise, emmental cheese, mixed greens with a potato chipotle salad on sourdough bread</p>
					</div>
					<div class="menu-gallery" data-aos="fade-up">
					  <div class="menu-gallery-container-salsas image-gallery">
					    <div class="swiper-wrapper">
					      <div class="swiper-slide"><img src="<?php echo IMG_PATH; ?>placeholder-home-gallery-01.jpg" /></div>
					      <div class="swiper-slide"><img src="<?php echo IMG_PATH; ?>placeholder-home-gallery-02.jpg" /></div>
					    </div>
					    <!-- Add Arrows -->
					    <div class="swiper-button-next is-hide-sm"></div>
					    <div class="swiper-button-prev is-hide-sm"></div>
					  </div>
					  <!-- If we need pagination -->
					  <div class="swiper-pagination swiper-pagination-salsas is-hide-lg"></div>
					</div>
					<div class="menu-item" data-aos="fade-up">
						<h3>
							<span class="name">chika pops</span>
							<span class="price">$16.50</span>
						</h3>
						<p class="description">Deliciously dippable.</p>
						<p class="ingredients">5 Chika flame-roasted chicken pops served with a variety of 3 homemade sauces</p>
					</div>
					<div class="menu-gallery" data-aos="fade-up">
					  <div class="menu-gallery-container-salsas image-gallery">
					    <div class="swiper-wrapper">
					      <div class="swiper-slide"><img src="<?php echo IMG_PATH; ?>placeholder-home-gallery-01.jpg" /></div>
					      <div class="swiper-slide"><img src="<?php echo IMG_PATH; ?>placeholder-home-gallery-02.jpg" /></div>
					    </div>
					    <!-- Add Arrows -->
					    <div class="swiper-button-next is-hide-sm"></div>
					    <div class="swiper-button-prev is-hide-sm"></div>
					  </div>
					  <!-- If we need pagination -->
					  <div class="swiper-pagination swiper-pagination-salsas is-hide-lg"></div>
					</div>
					<div class="menu-item" data-aos="fade-up">
						<h3>
							<span class="name">chika skewers</span>
							<span class="price">$16.50</span>
						</h3>
						<p class="description">Mama’s favorites—on a stick.</p>
						<p class="ingredients">3 Chika flame-roasted chicken kebabs served with grilled onions, tomatoes and poblano peppers</p>
					</div>
					<div class="menu-gallery" data-aos="fade-up">
					  <div class="menu-gallery-container-salsas image-gallery">
					    <div class="swiper-wrapper">
					      <div class="swiper-slide"><img src="<?php echo IMG_PATH; ?>placeholder-home-gallery-01.jpg" /></div>
					      <div class="swiper-slide"><img src="<?php echo IMG_PATH; ?>placeholder-home-gallery-02.jpg" /></div>
					    </div>
					    <!-- Add Arrows -->
					    <div class="swiper-button-next is-hide-sm"></div>
					    <div class="swiper-button-prev is-hide-sm"></div>
					  </div>
					  <!-- If we need pagination -->
					  <div class="swiper-pagination swiper-pagination-salsas is-hide-lg"></div>
					</div>
					<div class="menu-item" data-aos="fade-up">
						<h3>
							<span class="name">chika tacos</span>
							<span class="price">$16.50</span>
						</h3>
						<p class="description">Taco with your mouth full.</p>
						<p class="ingredients">3 Chika flame-roasted chicken tacos served on a corn tortilla with cilantro aioli & a chile de árbol (mango, pineapple, ciruela) or (chile tostado, ajo, ajonjolí) sauce</p>
					</div>
					<div class="menu-gallery" data-aos="fade-up">
					  <div class="menu-gallery-container-salsas image-gallery">
					    <div class="swiper-wrapper">
					      <div class="swiper-slide"><img src="<?php echo IMG_PATH; ?>placeholder-home-gallery-01.jpg" /></div>
					      <div class="swiper-slide"><img src="<?php echo IMG_PATH; ?>placeholder-home-gallery-02.jpg" /></div>
					    </div>
					    <!-- Add Arrows -->
					    <div class="swiper-button-next is-hide-sm"></div>
					    <div class="swiper-button-prev is-hide-sm"></div>
					  </div>
					  <!-- If we need pagination -->
					  <div class="swiper-pagination swiper-pagination-salsas is-hide-lg"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>