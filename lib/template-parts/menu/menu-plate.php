<section class="menu-row plate page-block">
	<div class="wrap">
		<div class="section-content">
			<div class="split-content">
				<div class="menu-title">
					<h2 data-aos="fade-right">Plate</h2>
					<img src="<?php echo IMG_PATH; ?>gfx-menu-chika-woman-01.jpg" class="chika-woman is-hide-sm" data-aos="zoom-out" data-bottom-top="transform: translateY(-15%);" data-top-bottom="transform: translateY(15%);" />
				</div>
				<div class="menu-items">
					<div class="menu-item" data-aos="fade-up">
						<h3>
							<span class="name">chika plate</span>
							<span class="price">$16.50</span>
						</h3>
						<p class="description">The full Mama Chika experience—without her telling you to eat seconds.</p>
						<p class="ingredients">1⁄2 Chika Fire Roasted Chicken, Mexican or Brown Rice, Black or Pinto Beans, Corn Tortillas, Roasted Garlic, Cambray Onions, Grilled Jalapenos, Add Guacamole +3</p>
					</div>
					<div class="menu-gallery" data-aos="fade-up">
					  <div class="menu-gallery-container-plate image-gallery">
					    <div class="swiper-wrapper">
					      <div class="swiper-slide"><img src="<?php echo IMG_PATH; ?>placeholder-home-gallery-01.jpg" /></div>
					      <div class="swiper-slide"><img src="<?php echo IMG_PATH; ?>placeholder-home-gallery-02.jpg" /></div>
					    </div>
					    <!-- Add Arrows -->
					    <div class="swiper-button-next is-hide-sm"></div>
					    <div class="swiper-button-prev is-hide-sm"></div>
					  </div>
					  <!-- If we need pagination -->
					  <div class="swiper-pagination swiper-pagination-plate is-hide-lg"></div>
					</div>
					<div class="menu-item" data-aos="fade-up">
						<h3>
							<span class="name">chika bowl</span>
							<span class="price">$16.50</span>
						</h3>
						<p class="description">When all the different flavors mix together. That’s what Mama loves about this one.</p>
						<p class="ingredients">Mixed Greens with arugula, Chika Fire Roasted Chicken, Fried Potato Strings with Tajin, Cilantro leafs, Cherry Tomatoes, Pumpkin seeds, Pumpkin seed dressing, Purple onion, Cucumbers, Chips, Add Guacamole +3</p>
					</div>
					<div class="menu-gallery" data-aos="fade-up">
					  <div class="menu-gallery-container-plate image-gallery">
					    <div class="swiper-wrapper">
					      <div class="swiper-slide"><img src="<?php echo IMG_PATH; ?>placeholder-home-gallery-01.jpg" /></div>
					      <div class="swiper-slide"><img src="<?php echo IMG_PATH; ?>placeholder-home-gallery-02.jpg" /></div>
					    </div>
					    <!-- Add Arrows -->
					    <div class="swiper-button-next is-hide-sm"></div>
					    <div class="swiper-button-prev is-hide-sm"></div>
					  </div>
					  <!-- If we need pagination -->
					  <div class="swiper-pagination swiper-pagination-plate is-hide-lg"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>