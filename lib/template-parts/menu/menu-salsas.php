<section class="menu-row salsas page-block">
	<div class="wrap">
		<div class="section-content">
			<div class="split-content">
				<div class="menu-title">
					<h2 data-aos="fade-right">Salsas</h2>
					<img src="<?php echo IMG_PATH; ?>gfx-menu-chika-woman-03.jpg" id="chika_03" class="chika-woman is-hide-sm" data-aos="zoom-out" data-bottom-top="transform: translateY(30px);" data-top-bottom="transform: translateY(-15px);" />
				</div>
				<div class="menu-items">
					<div class="menu-item" data-aos="fade-up">
						<h3>
							<span class="name">verde</span>
							<span class="price">$16.50</span>
						</h3>
						<p class="description">Fresh, with a beautiful kick.</p>
						<p class="ingredients">Tomatillos, serrano chiles, cilantro, garlic and, onion</p>
					</div>
					<div class="menu-item" data-aos="fade-up">
						<h3>
							<span class="name">roja asada</span>
							<span class="price">$16.50</span>
						</h3>
						<p class="description">Fiery. Full of flavor. And perfect with everything.</p>
						<p class="ingredients">Tomatoes, serrano chiles, cilantro, garlic and, onion</p>
					</div>
					<div class="menu-gallery" data-aos="fade-up">
					  <div class="menu-gallery-container-salsas image-gallery">
					    <div class="swiper-wrapper">
					      <div class="swiper-slide"><img src="<?php echo IMG_PATH; ?>placeholder-home-gallery-01.jpg" /></div>
					      <div class="swiper-slide"><img src="<?php echo IMG_PATH; ?>placeholder-home-gallery-02.jpg" /></div>
					    </div>
					    <!-- Add Arrows -->
					    <div class="swiper-button-next is-hide-sm"></div>
					    <div class="swiper-button-prev is-hide-sm"></div>
					  </div>
					  <!-- If we need pagination -->
					  <div class="swiper-pagination swiper-pagination-salsas is-hide-lg"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>