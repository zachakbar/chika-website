<section class="menu-row wings page-block">
	<div class="wrap">
		<div class="section-content">
			<div class="split-content">
				<div class="menu-title">
					<h2 data-aos="fade-right">Wings</h2>
				</div>
				<div class="menu-items">
					<div class="menu-item" data-aos="fade-up">
						<h3>
							<span class="name">chika wings</span>
							<span class="price">$16.50</span>
						</h3>
						<p class="description">Delicioso. That’s what Mama says.</p>
						<p class="ingredients">5 Jumbo Wings with Chika’s special recipe, Chile Aioli on the side</p>
					</div>
					<div class="menu-gallery" data-aos="fade-up">
					  <div class="menu-gallery-container-salsas image-gallery">
					    <div class="swiper-wrapper">
					      <div class="swiper-slide"><img src="<?php echo IMG_PATH; ?>placeholder-home-gallery-01.jpg" /></div>
					      <div class="swiper-slide"><img src="<?php echo IMG_PATH; ?>placeholder-home-gallery-02.jpg" /></div>
					    </div>
					    <!-- Add Arrows -->
					    <div class="swiper-button-next is-hide-sm"></div>
					    <div class="swiper-button-prev is-hide-sm"></div>
					  </div>
					  <!-- If we need pagination -->
					  <div class="swiper-pagination swiper-pagination-salsas is-hide-lg"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>