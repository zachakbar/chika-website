<?php
	include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/functions/helpers.php' );
	include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/general/header.php' );
?>

<section role="main" class="listen" data-page="listen">

	<?php

		include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/listen/hero.php' );
		include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/general/banner-tiles.php' );
		include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/listen/playlists.php' );

	?>

</section>

<?php include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/general/footer.php' ); ?>