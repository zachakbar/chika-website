<?php
	include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/functions/helpers.php' );
	include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/general/header.php' );
?>

<section role="main" class="menu" data-page="menu">

	<?php

		include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/menu/hero.php' );
		include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/general/banner-tiles.php' );
		include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/menu/menu-lead-in.php' );
		include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/menu/menu-plate.php' );
		include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/menu/menu-burrito.php' );
		include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/menu/menu-salads.php' );
		include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/menu/menu-misc.php' );
		include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/menu/menu-wings.php' );
		include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/menu/menu-salsas.php' );
		//include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/menu/menu-beverages.php' );

	?>

</section>

<?php include_once( $_SERVER['DOCUMENT_ROOT'].'/lib/template-parts/general/footer.php' ); ?>